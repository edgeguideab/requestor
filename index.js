const url = require('url');
const querystring = require('querystring');
const https = require('https');
const http = require('http');
const qs = require('querystring');

exports.get = function(path, options) {
  return exports.request(path, options);
};
exports.post = function(path, options) {
  options.method = 'POST';
  return exports.request(path, options);
};

exports.put = function(path, options) {
  options.method = 'PUT';
  return exports.request(path, options);
};
exports.delete = function(path, options) {
  options.method = 'DELETE';
  return exports.request(path, options);
};
exports.head = function(path, options) {
  options.method = 'HEAD';
  return exports.request(path, options);
};
exports.options = function(path, options) {
  options.method = 'OPTIONS';
  return exports.request(path, options);
};
exports.patch = function(path, options) {
  options.method = 'PATCH';
  return exports.request(path, options);
};

exports.request = function(path, options) {
  'use strict';
  return new Promise(function(resolve, reject) {
    if (arguments.length === 0) {
      return onError(500, 'Missing arguments');
    } else if (typeof path !== 'string') {
      return onError(500, 'First argument needs to be the path to request, however it was ' + path);
    }

    let urlParts = url.parse(path, true);
    let protocol = urlParts.protocol === 'https:' ? https : http;

    options = options || {};
    const queryJson = options.query ? Object.assign({}, urlParts.query, options.query) : urlParts.query;
    const queryString = qs.stringify(queryJson);
    options.hostname = urlParts.hostname;
    options.path = urlParts.pathname || '';
    options.path += queryString ? `?${queryString}` : '';
    options.port = urlParts.port || options.port || (urlParts.protocol === 'https:' ? 443 : 80);
    options.method = options.method || 'GET';
    options.headers = options.headers || {};
    options.timeout = options.timeout || 30000;

    if (options.cookies) {
      options.headers.Cookie = parseCookies(options.cookies);
    }

    let formData;
    if (options.body && typeof options.body.submit === 'function') { //assume it is a form-data object
      formData = options.body;
      delete options.body;
    }

    let contentType = options.headers['content-type'] || 'text/plain';
    delete options.headers['content-type'];

    if (options.form) {
      options.body = querystring.stringify(options.form);
      contentType = 'application/x-www-form-urlencoded';
      delete options.form;
    } else if (typeof options.body === 'object') {
      if (Buffer.isBuffer(options.body)) {
        contentType = 'application/octet-stream';
      } else {
        contentType = 'application/json';
        options.body = JSON.stringify(options.body);
      }
    }
    options.headers['Content-Type'] = options.headers['Content-Type'] || contentType;

    var responseBody = '';
    var timeout = 0;
    var request;
    if (formData) {
      formData.getLength(function (err, length) {
        if (err) {
          return onError(500, err);
        }
        options.headers['Content-Length'] = length;
        request = protocol.request(options, handleResponse);     
        formData.pipe(request);
        request.on('error', function(error) {
          onError(500, error);
        });
      });
    } else {
      request = protocol.request(options, handleResponse);
      request.on('error', function(error) {
        onError(500, error);
      });
      if (options.body) {
        request.end(options.body);
      } else {
        request.end();
      }
    }
    function handleResponse(response) {
      clearTimeout(timeout);
      response.on('data', function(chunk) {
        if (Buffer.isBuffer(chunk)) {
          if (!Buffer.isBuffer(responseBody)) {
            responseBody = Buffer.alloc(0);
          }
          responseBody = Buffer.concat([responseBody, chunk]);
        } else {
          responseBody += chunk;
        }
      });
      response.on('end', function() {
        if (response.headers['content-type'] && response.headers['content-type'].indexOf('application/json') !== -1) {
          if (responseBody.length === 0) {
            responseBody = undefined;
          } else {
            try {
              responseBody = JSON.parse(responseBody);
            } catch (e) {
              return onError(400, 'Expected json but got ' + responseBody);
            }
          }
        }
        if (response.statusCode >= 400) {
          return onError(response.statusCode, response.headers, responseBody);
        }
        resolve({
          status: response.statusCode,
          headers: response.headers,
          body: responseBody
        });
        return;
      });
    }

    timeout = setTimeout(function() {
      request.abort();
      onError(405, 'timeout exceeded');
    }, options.timeout);

    function parseCookies(cookie) {
      if (typeof cookie === 'string') {
        return cookie;
      }
      if (typeof cookie !== 'object') {
        return '';
      }
      if (Array.isArray(cookie)) {
        return cookie.join(';');
      } else {
        let cookieList = [];
        Object.keys(cookie).forEach(key => cookieList.push(`${key}=${cookie[key]}`));
        return cookieList.join(';');
      }
    }

    function onError(status, headers, errorText) {
      reject({
        status: status || 400,
        errorText: errorText || 'General error',
        headers: headers || {}
      });
    }
  });
};
